<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Melodics\CommonLaravel\Database\Migrations\PackageSchemaMigration;
use :uc:vendor\:uc:package\Facades\:uc:package;

class CreateExample:uc:packageTable extends PackageSchemaMigration
{
    protected function getPackageConnection(): string {
        return :uc:package::getConnection();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('example_:sc:package', function (Blueprint $table) {
            $table->char('ulid', 26)->primary();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('example_:sc:package');
    }
}
