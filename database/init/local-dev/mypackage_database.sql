CREATE USER :sc:package;

CREATE DATABASE :sc:package;

GRANT ALL PRIVILEGES ON :sc:package.* To ':sc:package' IDENTIFIED BY ':sc:package';