<?php

namespace :uc:vendor\:uc:package\Tests;

use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use :uc:vendor\:uc:package\Tests\Fixtures\:uc:packageTestContext;
use :uc:vendor\:uc:package\Tests\Fixtures\Expects\Expects;
use :uc:vendor\:uc:package\Tests\Fixtures\Given\Given;
use :uc:vendor\:uc:package\Tests\Fixtures\Then\Then;
use :uc:vendor\:uc:package\Tests\Fixtures\When\When;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use RegistersPackage;

    protected :uc:packageTestContext $context;
    protected Given $given;
    protected When $when;
    protected Then $then;
    protected Expects $expects;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->context = $context = new :uc:packageTestContext($this);
        $this->given = $context->given;
        $this->when = $context->when;
        $this->then = $context->then;
        $this->expects = $context->expects;
    }

    protected function tearDown(): void
    {
        $this->context->reset();
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $config = $app['config'];

        $app->useStoragePath(realpath(__DIR__.'/storage'));

        // Configure tested claims
        $config->set('auth.claims.restricted', [

        ]);

        $config->set('auth.claims.default', [

        ]);

        // Setup default database to use sqlite :memory:
        $config->set('database.default', 'testbench');
        $config->set(':sc:package.database.default', 'testbench');

        $config->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        $config->set('accounts.database.default', 'testbench_accounts');
        $config->set('passport.storage.database.connection', 'testbench_accounts');
        
        $config->set('database.connections.testbench_accounts', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => 'accounts_',
        ]);


        // make sure, our .env file is loaded
        $app->useEnvironmentPath(__DIR__.'/..');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::getEnvironmentSetUp($app);
    }

    public function getApp()
    {
        return $this->app;
    }

}

