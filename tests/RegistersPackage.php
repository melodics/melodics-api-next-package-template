<?php

namespace :uc:vendor\:uc:package\Tests;

use :uc:vendor\Accounts\AccountsServiceProvider;
use :uc:vendor\Accounts\Facades\Accounts;
use :uc:vendor\:uc:package\Facades\:uc:package;
use :uc:vendor\:uc:package\:uc:packageServiceProvider;

trait RegistersPackage
{
    protected function getPackageProviders($app)
    {
        return [AccountsServiceProvider::class, :uc:packageServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Accounts' => Accounts::class,
            ':uc:package' => :uc:package::class
        ];
    }
}
