<?php

namespace :uc:vendor\:uc:package\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use :uc:vendor\:uc:package\Tests\Fixtures\State;
use :uc:vendor\:uc:package\Tests\TestCase;
use :uc:vendor\CommonInterServiceCommunication\Events\:uc:package\Example:uc:packageCreated;
use :uc:vendor\CommonInterServiceCommunication\Events\:uc:package\Example:uc:packageCreatedEvent;
use :uc:vendor\CommonInterServiceCommunication\Events\:uc:package\Example:uc:packageUpdated;

class ServerToServerNotificationWebhookTests extends TestCase
{
    use RefreshDatabase;

    public function testServerToServerNotificationForARenewedSubscriptionThatWillNotAutoRenew()
    {
        $this->expects
            ->eventsToBeDispatched([
                Example:uc:packageUpdated::class
            ]);
        $this->given
            ->loggedInWithAccessTo([])
            ->anExample:uc:packageUpdateRequest()
            ->nExample:uc:package(1, function (int $index, State $state) {
                $user = $state->userEloquentModels()->getLastAddedEntity();
                $latestReceiptTransaction = $state->httpRequestBody['unified_receipt']['latest_receipt_info'][0];
                return [
                    'userUlid' => $user->ulid,
                    'legacyUserReference' => $user->legacyUserReference->legacy_user_uuid,
                    'productId' => $latestReceiptTransaction['product_id'],
                    'originalTransactionId' => $latestReceiptTransaction['original_transaction_id'],
                ];
            });
        $this->when
            ->aRequestIsMadeToTheServerToServerNotificationWebhook();
        $this->then
            ->apiResponseIsHTTP200OK()
            ->example:uc:packageUpdatedEventWasDispatchedWithUserReferences()
            ->example:uc:packageUpdatedEventWasDispatchedWithAutoRenewStatus(false);
    }
}
