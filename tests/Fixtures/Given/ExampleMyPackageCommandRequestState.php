<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Given;

use Illuminate\Support\Facades\Config;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;
use Ulid\Ulid;

trait Example:uc:packageCommandRequestState
{
    use HasState;

    public function anExample:uc:packageCommandRequest()
    {
        $this->state->httpRequestBody = [
            'commandUlid' => Ulid::generate(true),
            'ulid' => Ulid::generate(true),
        ];

        return $this;
    }

}