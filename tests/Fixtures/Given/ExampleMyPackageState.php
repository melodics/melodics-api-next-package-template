<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Given;

use Closure;
use :uc:vendor\:uc:package\Tests\Fixtures\Factories\Example:uc:packageFactory;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

trait Example:uc:packageState
{
    use HasState;

    /**
     * The Illuminate application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function nExample:uc:package(int $n = 1, Closure $overrideAttributes = null)
    {
        /** @var Example:uc:packageFactory */
        $factory = app(Example:uc:packageFactory::class);

        for ($ii=0; $ii < $n ; $ii++) {
            $entity = $factory->add($overrideAttributes ? $overrideAttributes($ii, $this->state) : []);
            $this->state->example:uc:packages()->add($entity->ulid, $entity);
        }
        
        return $this;
    }

}