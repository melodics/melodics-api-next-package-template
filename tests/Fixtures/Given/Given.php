<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Given;

use :uc:vendor\:uc:package\Tests\Fixtures\BDDTestComponent;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

class Given extends BDDTestComponent
{
    use HasState,
        HttpRequestState,
        AuthState,
        Example:uc:packageState,
        Example:uc:packageCommandRequestState;

    public function and(): Given
    {
        return $this;
    }
}