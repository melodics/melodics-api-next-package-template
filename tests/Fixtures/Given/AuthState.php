<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Given;

use Illuminate\Foundation\Testing\Concerns\InteractsWithAuthentication;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Foundation\Testing\Concerns\InteractsWithContainer;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\Concerns\InteractsWithTime;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\Concerns\MocksApplicationServices;
use Laravel\Passport\Passport;
use :uc:vendor\Accounts\Shared\Models\LegacyUserReference;
use :uc:vendor\Accounts\Shared\Models\User;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

trait AuthState
{
    use HasState,
    HttpRequestState,
    InteractsWithAuthentication,
    InteractsWithConsole,
    InteractsWithContainer,
    InteractsWithDatabase,
    InteractsWithExceptionHandling,
    InteractsWithSession,
    InteractsWithTime,
    MakesHttpRequests,
    MocksApplicationServices;

    public function loggedInWithAccessTo(Array $scopes = [])
    {
        $user = User::factory()
            ->has(LegacyUserReference::factory())
            ->create();

        Passport::actingAs(
            $user,
            $scopes
        );

        $this->state->userEloquentModels()->add($user->ulid, $user);

        return $this;
    }

}