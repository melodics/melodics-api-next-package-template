<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Given;


use Illuminate\Foundation\Testing\Concerns\InteractsWithAuthentication;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Foundation\Testing\Concerns\InteractsWithContainer;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\Concerns\InteractsWithTime;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\Concerns\MocksApplicationServices;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

trait HttpRequestState
{
    use HasState;

    protected function httpRequestHeaders(array $headers = [], $overwrite = false)
    {
        if ($overwrite) {
            $this->state->httpRequestHeaders = $headers;
        } else {
            $this->state->httpRequestHeaders = array_merge($this->state->httpRequestHeaders, $headers);
        }

        return $this;
    }

    protected function httpRequestBodyUnserialized(array $body = [], $overwrite = false)
    {
        if ($overwrite) {
            $this->state->httpRequestBody = $body;
        } else {
            $this->state->httpRequestBody = array_merge($this->state->httpRequestBody, $body);
        }
        
        return $this;
    }
}