<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Factories;

use Carbon\Carbon;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use :uc:vendor\:uc:package\Shared\Repositories\Example:uc:packageRepository;
use Ulid\Ulid;

class Example:uc:packageFactory extends BaseFactory
{
    public Example:uc:packageRepository $example:uc:packageRepository;

    public function __construct(Example:uc:packageRepository $example:uc:packageRepository)
    {
        $this->example:uc:packageRepository = $example:uc:packageRepository;
        parent::__construct();
    }

    public function add($attributeOverrides = [])
    {
        $example:uc:packageEntity = new Example:uc:packageEntity(
            isset($attributeOverrides['ulid']) ? $attributeOverrides['ulid'] : Ulid::generate(true),
        );

        $this->example:uc:packageRepository->add($example:uc:packageEntity);

        return $example:uc:packageEntity;
    }
}
