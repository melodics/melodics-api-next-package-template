<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Factories;

use Illuminate\Foundation\Testing\WithFaker;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use :uc:vendor\:uc:package\Shared\Models\Example:uc:package;
use :uc:vendor\:uc:package\Shared\Repositories\Example:uc:packageRepository;

abstract class BaseFactory
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }

}
