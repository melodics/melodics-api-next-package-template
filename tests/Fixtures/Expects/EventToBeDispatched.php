<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Expects;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\Concerns\InteractsWithAuthentication;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Foundation\Testing\Concerns\InteractsWithContainer;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\Concerns\InteractsWithTime;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\Concerns\MocksApplicationServices;
use Illuminate\Support\Facades\Event;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;
use :uc:vendor\:uc:package\Tests\Fixtures\HasTestCase;

trait EventToBeDispatched
{
    use HasState,
        HasTestCase,
        InteractsWithExceptionHandling,
        MakesHttpRequests;

    public function eventsToBeDispatched(array $events = null)
    {
        Event::fake($events);
    }

    public function eventsNotDispatched(array $events = null)
    {
        Event::fake($events);
    }
}