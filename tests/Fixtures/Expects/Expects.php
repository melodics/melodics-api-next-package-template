<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Expects;

use :uc:vendor\:uc:package\Tests\Fixtures\BDDTestComponent;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

class Expects extends BDDTestComponent
{
    use HasState,
        EventToBeDispatched;
}