<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\When;

use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

trait RequestingExampleMyPackageCommandToBeHandled
{
    use HasState,
    MakesHttpRequests;

    public function aRequestForTheExampleCommandToBeHandled()
    {
        $this->state->httpResponse = $this->post(
            ':sc:package/v1/exampleCommandRequest',
            $this->state->httpRequestBody,
            $this->state->httpRequestHeaders
        );

        return $this;
    }
}
