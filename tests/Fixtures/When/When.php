<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\When;

use :uc:vendor\:uc:package\Tests\Fixtures\BDDTestComponent;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

class When extends BDDTestComponent
{
    use HasState,
    RequestingExample:uc:packageCommandToBeHandled;
}