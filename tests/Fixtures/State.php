<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures;

use Illuminate\Testing\TestResponse;
use Ramsey\Collection\Map\TypedMap;

class State
{
    public array $httpRequestHeaders = ['accept' => 'application/json'];
    public array $httpRequestBody = [];
    public $result;
    public TestResponse $httpResponse;
    public $expected;
    public TypedMap $context;

    protected static function getInitalContext()
    {
        return [
            'UserEloquentModels' => new EntityContext(),
            'Example:uc:package' => new EntityContext(),
        ];
    }
    public function example:uc:packages(): EntityContext
    {
        return $this->context->get('Example:uc:package');
    }

    public function userEloquentModels(): EntityContext
    {
        return $this->context->get('UserEloquentModels');
    }


    public function __construct()
    {
        $this->context = new TypedMap('string', EntityContext::class, self::getInitalContext());
    }
}

class EntityContext {

    protected Array $keysInInsertOrder = [];
    protected $keyValueEntityStore = [];
    protected $keyValueMetaStore = [];

    public function add($key, $entity)
    {
        $this->keyValueEntityStore[$key] = $entity;
        $this->keyValueMetaStore[$key] = [
            'addedAt' => time()
        ];
        array_push($this->keysInInsertOrder, $key);
    }

    public function getEntityCount()
    {
        return count($this->keyValueEntityStore);
    }

    public function getEntity($key)
    {
        return $this->keyValueEntityStore[$key];
    }

    public function getLastAddedEntity()
    {
        $key = $this->keysInInsertOrder[count($this->keysInInsertOrder) - 1];
        return $this->getEntity($key);
    }
    
}
