<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures;

use Illuminate\Foundation\Testing\Concerns\InteractsWithAuthentication;
use Illuminate\Foundation\Testing\Concerns\InteractsWithConsole;
use Illuminate\Foundation\Testing\Concerns\InteractsWithContainer;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithExceptionHandling;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\Concerns\InteractsWithTime;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use Illuminate\Foundation\Testing\Concerns\MocksApplicationServices;
use :uc:vendor\:uc:package\Tests\Fixtures\Given\Given;
use :uc:vendor\:uc:package\Tests\Fixtures\When\When;
use :uc:vendor\:uc:package\Tests\Fixtures\Then\Then;
use :uc:vendor\:uc:package\Tests\Fixtures\Expects\Expects;
use :uc:vendor\:uc:package\Tests\TestCase;

class :uc:packageTestContext
{
    public string $initialStateSerialized;
    public Given $given;
    public When $when;
    public Then $then;
    public Expects $expects;

    protected State $state;

    // TODO Check this syntax operates when states default is overriden.
    public function __construct(
        TestCase $testCase,
        ?State $state = null
    )
    {
        $this->state = $state instanceof State ? $state : new State();

        // TODO: Refactor to DI Factories for these dependencies
        $this->given = new Given($testCase, $this->state);
        $this->when = new When($testCase, $this->state);
        $this->then = new Then($testCase, $this->state);
        $this->expects = new Expects($testCase, $this->state);

        $this->initialStateSerialized = serialize($this->state); 
    }

    public function reset(): void
    {
        $initialState = unserialize($this->initialStateSerialized);
        $this->state = $initialState;
    }

    public function setExpected($value)
    {
        $this->state->expected = $value;
    }

    public function setContext($key, $value)
    {
        $this->state->context->put($key, $value);
    }

    public function debugPrintContext()
    {
        // TODO improve output format
        print_r($this->state->context->toArray());
    }

    /**
     * TODO solve type hinting for autocompletion
     * @return array{0:Given,1:When,2:Then,3:Expects}
     */
    public function getGWTE()
    {
        return [$this->given, $this->when, $this->then, $this->expects];
    }

}

