<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Then;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;
use :uc:vendor\:uc:package\Tests\Fixtures\HasTestCase;

trait DatabaseContains
{
    use HasState,
        HasTestCase,
        InteractsWithDatabase;
}

class ArrayKeyCaseConverter
{
    public static function snakeCase(array $array): array
    {
        return array_map(
            function($item) {
                if (is_array($item)) {
                    $item = self::snakeCase($item);
                }
                
                return $item;
            },
            static::doSnakeCase($array)
        );
    }
 
    private static function doSnakeCase(array $array): array
    {
        $result = [];
 
        foreach ($array as $key => $value) {
            $key = strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', $key));
 
            $result[$key] = $value;
        }
 
        return $result;
    }
}