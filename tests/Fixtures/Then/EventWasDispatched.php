<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Then;

use Closure;
use Illuminate\Support\Facades\Event;
use :uc:vendor\Accounts\Shared\Models\User;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;
use :uc:vendor\:uc:package\Tests\Fixtures\HasTestCase;
use :uc:vendor\CommonInterServiceCommunication\Events\:uc:package\Example:uc:packageEvent;

trait EventWasDispatched
{
    use HasState,
        HasTestCase;

    public function example:uc:packageEventsNotDispatched()
    {
        Event::assertNotDispatched(Example:uc:packageEvent::class);
    }

    public function example:uc:packageEventWasDispatchedWithUserReferences()
    {
        Event::assertDispatched(
            Example:uc:packageEvent::class,
            function (Example:uc:packageEvent $event) {
                /** @var User */
                $userModel = $this->state->userEloquentModels()->getLastAddedEntity();
                
                $this->testCase->assertEquals($userModel->ulid, $event->getUserUlid());

                return true;
            }
        );

        return $this;
    }


}
