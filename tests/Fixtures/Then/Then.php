<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Then;

use :uc:vendor\:uc:package\Tests\Fixtures\BDDTestComponent;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;

class Then extends BDDTestComponent
{
    use HasState,
        ApiResponseContains,
        DatabaseContains,
        EventWasDispatched;
        
}

