<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures\Then;

use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use :uc:vendor\:uc:package\Tests\Fixtures\HasState;
use :uc:vendor\:uc:package\Tests\Fixtures\HasTestCase;

trait ApiResponseContains
{
    use HasState,
        HasTestCase,
        MakesHttpRequests;

    public function apiResponseIsHTTPUnauthorized()
    {
        $this->state->httpResponse->assertUnauthorized();
        return $this;
    }

    public function apiResponseIsHTTP200OK()
    {
        if ($this->state->httpResponse->baseResponse->getStatusCode() !== 200)
        {
            $this->state->httpResponse->dump();
        }
        $this->state->httpResponse->assertOk();
        return $this;
    }

    public function apiResponseIsHTTP204NoContent()
    {
        if ($this->state->httpResponse->baseResponse->getStatusCode() !== 204)
        {
            $this->state->httpResponse->dump();
        }
        $this->state->httpResponse->assertNoContent();
        return $this;
    }

    public function apiResponseIsHTTP400BadRequest()
    {
        if ($this->state->httpResponse->baseResponse->getStatusCode() !== 400)
        {
            $this->state->httpResponse->dump();
        }
        $this->state->httpResponse->assertStatus(400);
        return $this;
    }

    public function dumpHttpResponse()
    {
        $this->state->httpResponse->dump();

        return $this;
    }

}
