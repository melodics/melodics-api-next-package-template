<?php

namespace :uc:vendor\:uc:package\Tests\Fixtures;

use :uc:vendor\:uc:package\Tests\TestCase;
use Orchestra\Testbench\Concerns\Testing;
use PHPUnit_Framework_Constraint;

abstract class BDDTestComponent
{
    use HasState,
        HasTestCase,
        Testing;
     
    public function __construct(TestCase $testCase, State $state)
    {
        $this->testCase = $testCase;
        $this->state = $state;

        // Hack to allow use of the Orchestra\Testbench\Concerns\Testing trait
        $this->app = $testCase->getApp();
    }

    public function assertThat($value, $constraint, $message = ''): void
    {
        $this->testCase->assertThat($value, $constraint, $message);
    }

    // Hack to allow use of the Orchestra\Testbench\Concerns\Testing trait
    public function getEnvironmentSetUp()
    {
        // Noop
    }

    // Hack to allow use of the Orchestra\Testbench\Concerns\Testing trait
    public function refreshApplication()
    {
        // Noop
    }

    // Hack to allow use of the Orchestra\Testbench\Concerns\Testing trait
    public function setUpTraits()
    {
        // Noop
    }

}
