<?php

namespace :uc:vendor\:uc:package\Shared\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use :uc:vendor\:uc:package\Shared\Models\Example:uc:package;

class Example:uc:packageRepository
{

    /**
     * Creates a record for a new entity in the database
     */
    public function add(Example:uc:packageEntity $entity): void
    {
        Example:uc:package::fromDomainEntity($entity, true);
    }

    /**
     * Returns all Example:uc:package Entities as an array.
     * @return Collection<Example:uc:packageEntity>|Example:uc:packageEntity[]
     */
    public function getAll()
    {
        $collection = Example:uc:package::query()->get();

        return $collection->map(function($recordRule) {
            return Example:uc:packageEntity::fromEloquentModel($recordRule);
        });
    }
}