<?php

namespace :uc:vendor\:uc:package\Shared\Domain;

use :uc:vendor\:uc:package\Shared\Models\Example:uc:package;

class Example:uc:packageEntity
{

    public string $ulid;

    public static function fromEloquentModel(Example:uc:package $model): static
    {
        return new Example:uc:packageEntity(
            $model->ulid,
        );
    }

    public function __construct(
        string $ulid,
    )
    {
        $this->ulid = $ulid;
    }

}
