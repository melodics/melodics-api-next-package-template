<?php

namespace :uc:vendor\:uc:package\Shared\Models;

use Illuminate\Database\Eloquent\Model;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use :uc:vendor\:uc:package\Facades\:uc:package;
use Nedmas\Concerns\GeneratesUlid;

class Example:uc:package extends Model
{
    use GeneratesUlid;
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ulid';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    public function __construct(Array $attributes = [])
    {
        $this->connection = :uc:package::getConnection();
        parent::__construct($attributes);
    }

    public static function fromDomainEntity(Example:uc:packageEntity $entity, bool $create = false)
    {
        $attributes = [
            'ulid' => $entity->ulid,
        ];

        return $create ?
            static::create($attributes) :
            new static($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ulid',
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];
}