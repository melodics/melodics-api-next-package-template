<?php

namespace :uc:vendor\:uc:package\Query\ViewModels;

use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @OA\Schema()
 */
class Example:uc:packageViewModel
{
    public static function fromDomainEntity(Example:uc:packageEntity $entity): Example:uc:packageViewModel
    {
        return new Example:uc:packageViewModel(
            $entity->ulid,
        );
    }

    public function __construct(
        string $ulid
    )
    {
        $this->ulid = $ulid;
    }

    /**
     * ULID of the Example:uc:package
     * @OA\Property()
     */
    public string $ulid;

}

