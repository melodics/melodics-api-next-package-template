<?php

namespace :uc:vendor\:uc:package\Query\Resolvers;

use Illuminate\Support\Collection;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use :uc:vendor\:uc:package\Query\ViewModels\Example:uc:packageViewModel;
use :uc:vendor\:uc:package\Shared\Repositories\Example:uc:packageRepository;

class Example:uc:packageResolver
{
    protected Example:uc:packageRepository $example:uc:packageRepository;

    function __construct(Example:uc:packageRepository $example:uc:packageRepository)
    {  
        $this->example:uc:packageRepository = $example:uc:packageRepository;
    }

    /**
     * @return Collection<Example:uc:packageViewModel>
     */
    public function resolveAll()
    {
        return $this->transformDomainEntityCollectionToViewModelCollection(
            $this->example:uc:packageRepository->getAll()
        );
    }

    /**
     * @return Collection<Example:uc:packageViewModel>
     */
    public function transformDomainEntityCollectionToViewModelCollection($collection)
    {
        return $collection->map(function (Example:uc:packageEntity $recordCollection) {
            return Example:uc:packageViewModel::fromDomainEntity($recordCollection);
        });
    }
}