<?php

namespace :uc:vendor\:uc:package\Exceptions;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler as BaseExceptionHandler;
use Throwable;

class Handler extends BaseExceptionHandler
{
    protected ExceptionHandler $fallbackHandler;

    public function __construct(Container $container, ExceptionHandler $fallbackHandler)
    {
        $this->fallbackHandler = $fallbackHandler;
        parent::__construct($container);
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {   
        $this->renderable(function (:uc:packageException $exception) {
            return $this->renderPackageException($exception);
        });

        // Undefined method, not provided by NunoMaduro\Collision\Adapters\Laravel\ExceptionHandler
        if (get_class($this->fallbackHandler) === 'App\Exceptions\Handler')
        {
            /** @var BaseExceptionHandler */
            $handler = $this->fallbackHandler;
            $handler->register();
        }
    }

    protected function renderPackageException(:uc:packageException $exception)
    {
        return response([
            'message' => $exception->getMessage()
        ], $exception->getCode());
    }

    public function hasRenderCallback(Throwable $e)
    {
        foreach ($this->renderCallbacks as $renderCallback) {
            if (is_a($e, $this->firstClosureParameterType($renderCallback))) {
                return true;
            }
        }

        return false;
    }

    public function render($request, Throwable $e)
    {
        if ($this->hasRenderCallback($e))
        {
            return parent::render($request, $e);
        }
        else
        {
            return $this->fallbackHandler->render($request, $e);
        }
    }

    public function report(Throwable $e)
    {
        if (parent::shouldReport($e))
        {
            if (app()->bound('sentry') && $this->shouldReport($e)) {
                app('sentry')->captureException($e);
            }

            parent::report($e);
        }
        else if ($this->fallbackHandler->shouldReport($e))
        {
            $this->fallbackHandler->report($e);
        }
    }

    public function shouldReport(Throwable $e)
    {
        return parent::shouldReport($e) || $this->fallbackHandler->shouldReport($e);
    }

    public function shouldntReport(Throwable $e)
    {
        return parent::shouldntReport($e) && !$this->fallbackHandler->shouldReport($e);
    }
}
