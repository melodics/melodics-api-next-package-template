<?php

namespace :uc:vendor\:uc:package\Facades;

use Illuminate\Support\Facades\Facade;

class :uc:package extends Facade
{
    /**
     * Get the default database connections configuration key for the package.
     *
     * @return string
     */
    public static function getConnection()
    {
        return 'mysql_master_:sc:package';
    }
    
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ':cc:package';
    }
}
