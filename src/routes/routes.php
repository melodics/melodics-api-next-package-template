<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix(':cc:package')->group(function () {
    
    Route::prefix('v1')->middleware('api')->group(function () {
        // ~~ Authenticated Routes ~~
        Route::middleware(['auth:api'])->group(__DIR__.'/v1/protected.php');

        // ~~ Public Routes ~~
        Route::middleware([])->group(__DIR__.'/v1/public.php');

    });

});