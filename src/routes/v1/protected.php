<?php

use Illuminate\Support\Facades\Route;
use :uc:vendor\:uc:package\Command\:uc:packageCommandDispatcher;

/*
|--------------------------------------------------------------------------
| Protected API Routes
|--------------------------------------------------------------------------
*/


/**
 * @OA\Post(
 *      path="/",
 *      operationId="example:uc:packageEndpoint",
 *      tags={":uc:package"},
 *      summary="Example summary",
 *      description="Example description",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *      ),
 *      @OA\Response(response=400, description="Bad request"),
 *          security={
 *              {"api_key_security_example": {}}
 *          }
 *     )
 */

Route::post('/exampleCommandRequest', [:uc:packageCommandDispatcher::class, 'exampleCommandRequest']);