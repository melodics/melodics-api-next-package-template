<?php

use Illuminate\Support\Facades\Route;
use :uc:vendor\:uc:package\Query\Resolvers\Example:uc:packageResolver;

/*
|--------------------------------------------------------------------------
| Public API Routes
|--------------------------------------------------------------------------
*/

/**
 * @OA\Get(
 *      path="/",
 *      operationId="example:uc:packageEndpoint",
 *      tags={":uc:package"},
 *      summary="Example summary",
 *      description="Example description",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *      ),
 *      @OA\Response(response=400, description="Bad request"),
 *          security={
 *              {"api_key_security_example": {}}
 *          }
 *     )
 */

Route::get('/example:uc:package', [Example:uc:packageResolver::class, 'resolveAll']);