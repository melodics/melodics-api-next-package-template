<?php

/**
 * NOTE: Any events that this package raises should be moved and committed to the Melodics/CommonInterServiceCommunication package.
 */

 namespace Melodics\CommonInterServiceCommunication\Events\:uc:package;

use Melodics\CommonInterServiceCommunication\Events\Payload;
use Melodics\CommonInterServiceCommunication\Events\SynchronousEvent;

class ExampleSynchronous:uc:packageEvent extends SynchronousEvent
{
    public static $schemaVersion = 1;

    public static function dispatchSynchronously(
        string $traceUlid,
        array $traceBreadcrumbs,
        string $exampleEntityUlid
    )
    {
        $payload = new Payload(
            $traceUlid,
            $traceBreadcrumbs,
            static::$schemaVersion,
            [
                'exampleEntityUlid' => $exampleEntityUlid,
                'raisedAt' => Carbon::now()->toIso8601String(),
            ]
        );

        $event = new static($payload);

        $dispatcher = static::resolveDispatcher();

        $dispatcher->dispatchSynchronously($event);
    }

    public function getExampleUlid(): string
    {
        return $this->payload->payload['exampleEntityUlid'];
    }
}