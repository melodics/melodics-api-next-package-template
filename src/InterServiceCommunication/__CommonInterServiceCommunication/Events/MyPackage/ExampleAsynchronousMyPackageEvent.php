<?php

/**
 * NOTE: Any events that this package raises should be moved and committed to the Melodics/CommonInterServiceCommunication package.
 */

namespace Melodics\CommonInterServiceCommunication\Events\:uc:package;

use Carbon\Carbon;
use Melodics\CommonInterServiceCommunication\Events\AsynchronousEvent;
use Melodics\CommonInterServiceCommunication\Events\Payload;

class ExampleAsynchronous:uc:packageEvent extends AsynchronousEvent
{
    protected static $schemaVersion = 1;

    public static string $awsSnsConfigurationPath = ':sc:package.events.broadcast.:sc:package_events.aws_sns';

    public static string $awsClientConfigurationBasePath = ':sc:package.aws';

    public static function dispatchAsynchronouslyViaAwsSns(
        string $traceUlid,
        array $traceBreadcrumbs,
        string $exampleEntityUlid
    )
    {
        $payload = new Payload(
            $traceUlid,
            $traceBreadcrumbs,
            static::$schemaVersion,
            [
                'exampleEntityUlid' => $exampleEntityUlid,
                'raisedAt' => Carbon::now()->toIso8601String(),
            ]
        );

        $event = new static($payload);

        $dispatcher = static::resolveDispatcher();

        $dispatcher->dispatchAsynchronouslyViaAwsSns($event);
    }

    public function getEventSequenceGroupIdentifier(): string
    {
        return $this->payload->payload['exampleEntityUlid'];
    }
    
    public function getExampleUlid(): string
    {
        return $this->payload->payload['exampleEntityUlid'];
    }
    
}