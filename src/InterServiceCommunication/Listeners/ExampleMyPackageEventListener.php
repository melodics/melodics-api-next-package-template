<?php

namespace Melodics\Subscriptions\InterServiceCommunication\Listeners;

use Melodics\CommonInterServiceCommunication\Events\Listener;
use Melodics\CommonInterServiceCommunication\Events\OtherMelodicsService\OtherMelodicsServiceEvent;
use Melodics\CommonInterServiceCommunication\Events\:uc:package\ExampleAsynchronous:uc:packageEvent;
use :uc:vendor\:uc:package\Shared\Repositories\Example:uc:packageRepository;
use :uc:vendor\:uc:package\Shared\Domain\Example:uc:packageEntity;
use Ulid\Ulid;

class Example:uc:packageEventListener extends Listener
{

    public static function resolveExample:uc:packageRepository(): Example:uc:packageRepository
    {
        return app(Example:uc:packageRepository::class);
    }

    public function handleOtherMelodicsServiceEvent(OtherMelodicsServiceEvent $event): void
    {
        $this->traceLog(' Event Handler:', $event);

        // Do something based on data in the event ...
        if ($event->getExampleEventProperty() === '')
        {
            // Like create something an entity record in the database
            $example:uc:package:Entity = new Example:uc:packageEntity(Ulid::generate(true));

            $repository = static::resolveExample:uc:packageRepository();

            $repository->add($example:uc:package:Entity);

            // And / or raise another event
            ExampleAsynchronous:uc:packageEvent::dispatchAsynchronouslyViaAwsSns(
                $event->getTraceUlid(),
                array_merge($event->getTraceBreadcrumbs(), [static::class.(isset($this) ? '->' : '::').__FUNCTION__]),
                $exampleEntityUlid
            );
        }
    }

}