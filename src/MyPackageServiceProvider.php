<?php

namespace :uc:vendor\:uc:package;

use Illuminate\Contracts\Container\Container;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use :uc:vendor\:uc:package\Exceptions\Handler;

class :uc:packageServiceProvider extends ServiceProvider
{
    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');

        // Publishing and migrations are only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // Auth config to be published
        // This configuration will need to be duplicated in the accounts package for any physical microservice
        $this->mergeConfigFrom(__DIR__.'/../config/:sc:package.auth.claims.restricted.php', 'auth.claims.restricted');
        $this->mergeConfigFrom(__DIR__.'/../config/:sc:package.auth.claims.default.php', 'auth.claims.default');

        // Base package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/:sc:package.php', ':sc:package');

        // Database connection configuration
        $this->mergeConfigFrom(__DIR__.'/../config/:sc:package.database.connections.php', 'database.connections');

        // Register mechanism for token based authorization
        $this->ensureTokenClaimsCheckingMiddlewareIsRegistered();

        // Register package exception handler
        $this->registerPackageExceptionHandler();

        // Register package event listeners
        $this->app->register(:uc:packageEventServiceProvider::class);

        // Register the service the package provides.
        $this->app->singleton(':cc:package', function ($app) {
            return new :uc:package;
        });
    }

    protected function ensureTokenClaimsCheckingMiddlewareIsRegistered()
    {
        /** @var Router */
        $router = $this->app->get('router');
        
        if (!isset($router->getMiddleware()['scopes']))
        {
            $router->aliasMiddleware('scopes', \Laravel\Passport\Http\Middleware\CheckScopes::class);
        }

        if (!isset($router->getMiddleware()['scope']))
        {
            $router->aliasMiddleware('scope', \Laravel\Passport\Http\Middleware\CheckForAnyScope::class);
        }
    }

    protected function registerPackageExceptionHandler()
    {
        $this->app->extend('Illuminate\Contracts\Debug\ExceptionHandler', function ($service, $app) {
            return new Handler($app->get(Container::class), $service);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [':cc:package'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Publishes the local dev init db sql script
        $this->publishes([
            __DIR__.'/../database/init/local-dev/:sc:package_database.sql' => base_path('database/init/local-dev'),
        ], ':lc:package.database.init.local-dev');

        // Registering package commands.
        // $this->commands([]);
    }

}
