<?php

namespace :uc:vendor\:uc:package\Command\Validators;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Validation\Validator;
use :uc:vendor\:uc:package\Command\Commands\Example:uc:packageCommand;

class Example:uc:packageCommandValidator
{

    use ValidatesRequests;

    protected Validator $validator;

    public array $rules = [
        
    ];

    public array $messages = [

    ];

    public array $customAttributes = [

    ];

    /**
     * Validate the given command with the given rules.
     *
     * @param  Example:uc:packageCommand $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(
        Example:uc:packageCommand $command
    )
    {
        $this->getValidationFactory()->make(
            (array) $command, $this->rules, $this->messages, $this->customAttributes
        )->validate();
    }

    /**
     * Get a validation factory instance.
     *
     * @return \Illuminate\Contracts\Validation\Factory
     */
    protected function getValidationFactory()
    {
        return app(Factory::class);
    }
}