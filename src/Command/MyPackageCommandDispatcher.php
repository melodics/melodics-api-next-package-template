<?php

namespace :uc:vendor\:uc:package\Command;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use :uc:vendor\:uc:package\Command\Commands\Example:uc:packageCommand;
use :uc:vendor\:uc:package\Command\Handlers\Example:uc:packageCommandValidator;
use :uc:vendor\:uc:package\Command\Validators\Example:uc:packageCommandHandler;

class :uc:packageCommandDispatcher
{

    public function exampleCommandRequest(Request $request)
    {
        return $this->processCommandRequestSynchronously(
            Example:uc:packageCommand::class,
            Example:uc:packageCommandValidator::class,
            Example:uc:packageCommandHandler::class,
            $request
        );
    }

    /**
     * @throws ValidationException
     */
    protected function processCommandRequestSynchronously(string $commandClassName, string $commandValidatorClassName, string $commandHandlerClassName, Request $request)
    {
        $command = ($commandClassName)::fromLaravelRequest($request);

        app($commandValidatorClassName)->validate($command);

        app($commandHandlerClassName)->handle($command);

        return response()->json('', 204);
    }
}