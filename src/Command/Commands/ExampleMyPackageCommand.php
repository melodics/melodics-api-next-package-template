<?php

namespace :uc:vendor\:uc:package\Command\Commands;

use Illuminate\Http\Request;

/**
 * @OA\Schema()
 */
class Example:uc:packageCommand
{

    public static function fromLaravelRequest(Request $request)
    {
        return new static(
            $request->get('commandUlid'),
            $request->get('exampleUlid')
        );
    }

    public function __construct(
        string $commandUlid,
        string $exampleUlid
    )
    {
        $this->commandUlid = $commandUlid;
        $this->exampleUlid = $exampleUlid;
    }

    /**
     * CommandUlid used for tracability and synthetic idempotence
     * @OA\Property()
     */
    public string $commandUlid;

    /**
     * Example entity ULID
     * @OA\Property()
     */
    public string $exampleUlid;

}