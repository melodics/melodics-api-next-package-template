<?php

namespace :uc:vendor\:uc:package\Command\Handlers;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use :uc:vendor\:uc:package\Command\Commands\Example:uc:packageCommand;
use :uc:vendor\:uc:package\Shared\Repositories\:uc:packageRepository;

class Example:uc:packageCommandHandler
{

    protected Example:uc:packageRepository $example:uc:packageRepository;

    public function __construct(Example:uc:packageRepository $example:uc:packageRepository)
    {
        $this->example:uc:packageRepository = $example:uc:packageRepository;
    }

    public function handle(Example:uc:packageCommand $command)
    {
        try {
            DB::beginTransaction();

            // Make stateful changes, raise events

            DB::commit();
        }
        catch (Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }

    }
}