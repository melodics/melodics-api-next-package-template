<?php

namespace Melodics\:uc:package;

use Illuminate\Support\Facades\Config;
use Melodics\CommonLaravel\ServiceProviders\PackageEventServiceProvider;

class :uc:packageEventServiceProvider extends PackageEventServiceProvider
{

    /**
     * Get the listener directories that should be used to discover events.
     *
     * @return array
     */
    protected function discoverEventsWithin()
    {
        return Config::get(':sc:package.events.listeners.paths');
    }

}
