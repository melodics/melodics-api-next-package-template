<?php
// For services deployed independently of the core API Next runtime, this configuration needs to be duplicated in the Accounts service.
return [
    ':cc:package:admin:read' => 'Grants access to read all resources managed by the :uc:package service.',
    ':cc:package:admin:write' => 'Grants access to write all resources managed by the :uc:package service.',
];