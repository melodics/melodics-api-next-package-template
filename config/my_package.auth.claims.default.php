<?php

return [
    ':cc:package:owned:all' => 'Grants access to all user owned resources managed by the :uc:package service.'
];
