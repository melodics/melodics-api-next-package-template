<?php

return [
    'aws' => [
        'key' => env(':UC:PACKAGE_AWS_KEY', ''),
        'secret' => env(':UC:PACKAGE_AWS_SECRET', ''),
        'region' => env(':UC:PACKAGE_AWS_REGION', '')
    ],
    'database' => [
        'default' => env(':UC:PACKAGE_DB_CONNECTION', 'mysql_master_gamification'),
    ],
    'events' => [
        'listeners' => [
            'paths' => [
                __DIR__.'/../src/InterServiceCommunication/Listeners/'
            ],
        ],
        'broadcast' => [
            'subscriptions_events' => [
                'aws_sns' => [
                    'topic_arn' => env('SUBSCRIPTIONS_EVENTS_BROADCAST_AWS_SNS_SUBSCRIPTIONS_EVENTS_TOPIC_ARN')
                ]
            ]
        ]
    ],
    'storage' => [
        's3' => [
            'region' => env(':UC:PACKAGE_STORAGE_S3_REGION', ''),
        ],
        'example_feature' => [
            'example_data_type' => [
                's3Bucket' => env(':UC:PACKAGE_STORAGE_EXAMPLE_FEATURE_EXAMPLE_DATA_TYPE_S3_BUCKET', ''),
                'basePath' => env(':UC:PACKAGE_STORAGE_EXAMPLE_FEATURE_EXAMPLE_DATA_TYPE_BASE_PATH', ''),
            ]
        ]
    ]
];