<?php

return [
    'mysql_master_:sc:package' => [
        'driver' => 'mysql',
        'url' => env(':UC:PACKAGE_DATABASE_URL'),
        'host' => env(':UC:PACKAGE_DB_HOST', '127.0.0.1'),
        'port' => env(':UC:PACKAGE_DB_PORT', '3306'),
        'database' => env(':UC:PACKAGE_DB_DATABASE', ':sc:package'),
        'username' => env(':UC:PACKAGE_DB_USERNAME', ':sc:package'),
        'password' => env(':UC:PACKAGE_DB_PASSWORD', ''),
        'unix_socket' => env(':UC:PACKAGE_DB_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => true,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],
];