# :uc:package

This is where your description should go.

## Installation

Via Composer

``` bash
$ composer require :lc:vendor/:lc:package
```

## Usage

### Architecture and Design Principles

#### Public Interfaces for (Logical) Microservices
All Melodics Services have a public interface that is limited to the following:

When communicating solely with other services within the core API Next runtime:
 - Events that are dispatched and handled synchronously
 - A Laravel Facade

When communicating with services or clients external to the core API Next runtime:
 - Events that are broadcast via SNS, where systems external to API Next need to respond to the events
 - A public CQRS compliant HTTP API
 - A private CQRS compliant internal HTTP API

NB: In special circumstances, for example authentication, CQRS is not an appropriate limitation to impose.

#### Testing Principles

Mock dependencies only when it becomes too expensive (time, resources) or complex (configuration or test data creation) not to do so.

Prefer tests that have only one assertion - for API tests HTTP status code assertions are not included in this count.

When there are a significant number of permutations to test, this should be done at the most granular level that is practical, e.g. unit tests.

The most common permutations should be tested at the highest level practical, e.g. e2e tests.

Do not write tests that confirm things that could be provable through the type system or static analysis. Application code should be refactored to utilise the type system and/or static analysis where possible to eliminate the need for these types of tests.

During development, testing and production operation of the software, any bugs or errors that are discovered should be reproduced in an automated test before being fixed.

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email :author_email instead of using the issue tracker.

## Credits

- [:author_name][link-author]
- [All Contributors][link-contributors]

## License

:license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/:lc:vendor/:lc:package.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/:lc:vendor/:lc:package.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/:lc:vendor/:lc:package/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/:lc:vendor/:lc:package
[link-downloads]: https://packagist.org/packages/:lc:vendor/:lc:package
[link-travis]: https://travis-ci.org/:lc:vendor/:lc:package
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/:lc:vendor
[link-contributors]: ../../contributors
